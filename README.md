# data_science_task

Technical task for the data scientist position (ID:001)

## Data
In the `data` folder you will find the following two data sets:
- The `calibration.xlsx` file contains the absorbance readings of a standard (S1) at different dilutions and the absorbance reading of the blanks (solvent). There are 3 replicates for each dilution. The undiluted standard contains 50mg/L of pigment.
- The `sample.xlsx` file contains the absorance readings of a sample (X1) measured in triplicates and the absorbance readings of the blanks (solvent). 

## Task Outline
Write an executable `Python` script that addresses the following two parts:

#### Part 1
Visualize the provided calibration data (`calibration.xlsx`) in a figure. Plot the wavelengths [220, 222, ..., 800] against the measured absorbances for the different dilutions. You may want to inspect both raw and the blank corrected data. 

#### Part 2
Using the calibration data from Part 1, implement a mathematical model to calculate the pigment concentration in a given sample. Clearly state the assumptions of your mathematical model in the implementation section of this README. Calculate the pigment concentration of the sample provided in the sample file (`sample.xlsx`).

## Constraints

The solution should run in `Python >= 3.7`. We encourage you to follow the PEP8 style guides.

Please make use of existing data science python libraries such as `numpy`, `pandas`, and `matplotlib` to implement your solutions. You should provide detailed instructions on installation requirements to run your code (e.g. package versions). We love `poetry` for package management, but feel free to use any tools you like. 

## Implementation 
*This section should be completed by the candidate*

- [ ] Please leave notes on how to run your code here, including any installation requirements.

- [ ] Please leave notes on the assumptions that your proposed mathematical model makes.